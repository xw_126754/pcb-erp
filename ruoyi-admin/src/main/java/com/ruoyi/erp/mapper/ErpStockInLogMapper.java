package com.ruoyi.erp.mapper;

import java.util.List;
import com.ruoyi.erp.domain.ErpStockInLog;

/**
 * 入库日志Mapper接口
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public interface ErpStockInLogMapper 
{
    /**
     * 查询入库日志
     * 
     * @param id 入库日志ID
     * @return 入库日志
     */
    public ErpStockInLog selectErpStockInLogById(String id);

    /**
     * 查询入库日志列表
     * 
     * @param erpStockInLog 入库日志
     * @return 入库日志集合
     */
    public List<ErpStockInLog> selectErpStockInLogList(ErpStockInLog erpStockInLog);

    /**
     * 新增入库日志
     * 
     * @param erpStockInLog 入库日志
     * @return 结果
     */
    public int insertErpStockInLog(ErpStockInLog erpStockInLog);

    /**
     * 修改入库日志
     * 
     * @param erpStockInLog 入库日志
     * @return 结果
     */
    public int updateErpStockInLog(ErpStockInLog erpStockInLog);

    /**
     * 删除入库日志
     * 
     * @param id 入库日志ID
     * @return 结果
     */
    public int deleteErpStockInLogById(String id);

    /**
     * 批量删除入库日志
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteErpStockInLogByIds(String[] ids);
}
