package com.ruoyi.erp.service.impl;

import java.util.List;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.erp.mapper.ErpStockInLogMapper;
import com.ruoyi.erp.domain.ErpStockInLog;
import com.ruoyi.erp.service.IErpStockInLogService;
import com.ruoyi.common.core.text.Convert;

/**
 * 入库日志Service业务层处理
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Service
public class ErpStockInLogServiceImpl implements IErpStockInLogService 
{
    @Autowired
    private ErpStockInLogMapper erpStockInLogMapper;

    /**
     * 查询入库日志
     * 
     * @param id 入库日志ID
     * @return 入库日志
     */
    @Override
    public ErpStockInLog selectErpStockInLogById(String id)
    {
        return erpStockInLogMapper.selectErpStockInLogById(id);
    }

    /**
     * 查询入库日志列表
     * 
     * @param erpStockInLog 入库日志
     * @return 入库日志
     */
    @Override
    public List<ErpStockInLog> selectErpStockInLogList(ErpStockInLog erpStockInLog)
    {
        return erpStockInLogMapper.selectErpStockInLogList(erpStockInLog);
    }

    /**
     * 新增入库日志
     * 
     * @param erpStockInLog 入库日志
     * @return 结果
     */
    @Override
    public int insertErpStockInLog(ErpStockInLog erpStockInLog)
    {
        erpStockInLog.setId(IdUtils.fastSimpleUUID());
        erpStockInLog.setCreateTime(DateUtils.getNowDate());
        return erpStockInLogMapper.insertErpStockInLog(erpStockInLog);
    }

    /**
     * 修改入库日志
     * 
     * @param erpStockInLog 入库日志
     * @return 结果
     */
    @Override
    public int updateErpStockInLog(ErpStockInLog erpStockInLog)
    {
        erpStockInLog.setUpdateTime(DateUtils.getNowDate());
        return erpStockInLogMapper.updateErpStockInLog(erpStockInLog);
    }

    /**
     * 删除入库日志对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteErpStockInLogByIds(String ids)
    {
        return erpStockInLogMapper.deleteErpStockInLogByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除入库日志信息
     * 
     * @param id 入库日志ID
     * @return 结果
     */
    @Override
    public int deleteErpStockInLogById(String id)
    {
        return erpStockInLogMapper.deleteErpStockInLogById(id);
    }
}
