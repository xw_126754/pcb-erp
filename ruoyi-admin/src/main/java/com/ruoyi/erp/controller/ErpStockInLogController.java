package com.ruoyi.erp.controller;

import java.util.List;

import com.ruoyi.erp.domain.ErpStock;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.erp.domain.ErpStockInLog;
import com.ruoyi.erp.service.IErpStockInLogService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 入库日志Controller
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Controller
@RequestMapping("/erp/erpStockInLog")
public class ErpStockInLogController extends BaseController
{
    private String prefix = "erp/erpStockInLog";

    @Autowired
    private IErpStockInLogService erpStockInLogService;

    @RequiresPermissions("erp:erpStockInLog:view")
    @GetMapping()
    public String erpStockInLog()
    {
        return prefix + "/erpStockInLog";
    }

    /**
     * 查询入库日志列表
     */
    @RequiresPermissions("erp:erpStockInLog:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ErpStockInLog erpStockInLog)
    {
        startPage();
        List<ErpStockInLog> list = erpStockInLogService.selectErpStockInLogList(erpStockInLog);
        return getDataTable(list);
    }

    /**
     * 导出入库日志列表
     */
    @RequiresPermissions("erp:erpStockInLog:export")
    @Log(title = "入库日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ErpStockInLog erpStockInLog)
    {
        List<ErpStockInLog> list = erpStockInLogService.selectErpStockInLogList(erpStockInLog);
        ExcelUtil<ErpStockInLog> util = new ExcelUtil<ErpStockInLog>(ErpStockInLog.class);
        return util.exportExcel(list, "erpStockInLog");
    }

    /**
     * 新增入库日志
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存入库日志
     */
    @RequiresPermissions("erp:erpStockInLog:add")
    @Log(title = "入库日志", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ErpStockInLog erpStockInLog)
    {
        return toAjax(erpStockInLogService.insertErpStockInLog(erpStockInLog));
    }

    /**
     * 修改入库日志
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        ErpStockInLog erpStockInLog = erpStockInLogService.selectErpStockInLogById(id);
        mmap.put("erpStockInLog", erpStockInLog);
        return prefix + "/edit";
    }

    /**
     * 修改保存入库日志
     */
    @RequiresPermissions("erp:erpStockInLog:edit")
    @Log(title = "入库日志", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ErpStockInLog erpStockInLog)
    {
        return toAjax(erpStockInLogService.updateErpStockInLog(erpStockInLog));
    }

    /**
     * 删除入库日志
     */
    @RequiresPermissions("erp:erpStockInLog:remove")
    @Log(title = "入库日志", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(erpStockInLogService.deleteErpStockInLogByIds(ids));
    }

    /**
     * 查看详细
     */
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") String id, ModelMap mmap) {
        ErpStockInLog erpStockInLog = erpStockInLogService.selectErpStockInLogById(id);
        mmap.put("erpStockInLog", erpStockInLog);
        return prefix + "/detail";
    }
}
